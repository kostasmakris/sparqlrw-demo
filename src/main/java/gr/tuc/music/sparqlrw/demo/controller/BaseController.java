/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.demo.controller;

import com.hp.hpl.jena.query.QueryFactory;
import gr.tuc.music.anapsid.wrapper.AnapsidWrapper;
import gr.tuc.music.sparqlrw.demo.bean.Query;
import gr.tuc.music.sparqlrw.demo.bean.ResultSet;
import gr.tuc.music.sparqlrw.demo.listener.BaseApplicationListener;
import gr.tuc.music.sparqlrw.query.rewriting.QueryRewriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
@Controller
public class BaseController {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(BaseController.class);

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about() {
        return "about";
    }

    @RequestMapping(value = "/rewriteQuery", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    @ResponseBody
    public Query rewriteQuery(@RequestBody Query query) {
        QueryRewriter rewriter = new QueryRewriter();

        if (query == null) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        com.hp.hpl.jena.query.Query jenaInputQuery = QueryFactory.create(query.getQuery());
        if (jenaInputQuery == null) {
            log.warn("Input query failed to be parsed.");
            return null;
        }

        com.hp.hpl.jena.query.Query jenaOutputQuery = rewriter.rewrite(jenaInputQuery, BaseApplicationListener.model, BaseApplicationListener.ontEndpointMap);

        // Since the method is annotated with @ResponseBody the internal view resolver
        // is not involved and the returned data do not get mapped to a view.
        return new Query(jenaOutputQuery.toString());
    }

    @RequestMapping(value = "/executeQuery", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResultSet executeQuery(@RequestBody Query query) {
        AnapsidWrapper anapsid = new AnapsidWrapper(BaseApplicationListener.endpointDescLocation);

        if (query == null) {
            log.warn("Input parameters cannot be null.");
            return null;
        }

        gr.tuc.music.anapsid.wrapper.ResultSet rs = anapsid.executeQuery(query.getQuery());
        if (rs == null) {
            return null;
        }

        // Since the method is annotated with @ResponseBody the internal view resolver
        // is not involved and the returned data do not get mapped to a view.
        return new ResultSet(rs.getFilename(), rs.getVarList());
    }

    @RequestMapping(value = "/fetchResults/{resultSetId}", method = RequestMethod.GET)
    @ResponseBody
    public void fetchResults(@PathVariable String resultSetId, HttpServletResponse response) {
        File file = FileUtils.getFile(FileUtils.getTempDirectoryPath(), resultSetId + ".tmp");

        try {
            if (file == null) {
                response.sendError(404);
                return;
            }

            gr.tuc.music.anapsid.wrapper.ResultSet rs = new gr.tuc.music.anapsid.wrapper.ResultSet(new ArrayList<String>(), file);

            // convert Anapsid results to json
            File jsonFile = rs.toJSON();
            if (jsonFile == null) {
                response.sendError(404);
                return;
            }

            response.setContentLength(new Long(jsonFile.length()).intValue());
            response.setContentType(MediaType.APPLICATION_JSON);

            try {
                FileCopyUtils.copy(new FileInputStream(jsonFile), response.getOutputStream());
            } catch (IOException ex) {
                log.warn("IO Exception occured.", ex);
            }

        } catch (IOException ex) {
            log.warn("IO Exception occured.", ex);
        }
    }
}
