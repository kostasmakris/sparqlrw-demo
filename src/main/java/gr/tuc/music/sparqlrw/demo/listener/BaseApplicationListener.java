/*
 * Copyright 2014 TUC/MUSIC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.tuc.music.sparqlrw.demo.listener;

import gr.tuc.music.sparqlrw.mapping.model.Deserializer;
import gr.tuc.music.sparqlrw.mapping.model.MappingModel;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * Initializes shared resources on application startup.
 *
 * @author Konstantinos Makris <makris@ced.tuc.gr>
 */
public class BaseApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(BaseApplicationListener.class);

    public static MappingModel model;
    public static HashMap<String, List<String>> ontEndpointMap;
    public static String endpointDescLocation;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        File mappingsXml = FileUtils.toFile(getClass().getClassLoader().getResource("mappings.xml"));
        Deserializer deserializer = new Deserializer();

        List<String> neEndpoints = new ArrayList<String>();
        List<String> colEndpoints = new ArrayList<String>();
        List<String> uniprotEndpoints = new ArrayList<String>();
        List<String> dbpediaEndpoints = new ArrayList<String>();
        List<String> geonamesEndpoints = new ArrayList<String>();

        neEndpoints.add("http://nhmc.collections.natural-europe.eu:8890/sparql");
        neEndpoints.add("http://mnhnl.collections.natural-europe.eu:8890/sparql");
        neEndpoints.add("http://jme.collections.natural-europe.eu:8890/sparql");
        neEndpoints.add("http://tnhm.collections.natural-europe.eu:8890/sparql");
        neEndpoints.add("http://ac.collections.natural-europe.eu:8890/sparql");
        neEndpoints.add("http://hnhm.collections.natural-europe.eu:8890/sparql");
        colEndpoints.add("http://natural-europe.tuc.gr/sw/col/sparql");
        uniprotEndpoints.add("http://beta.sparql.uniprot.org/sparql");
        dbpediaEndpoints.add("http://dbpedia.org/sparql");
        geonamesEndpoints.add("http://factforge.net/sparql");

        // load the mapping model
        model = deserializer.deserialize(mappingsXml);

        // load endpoints
        ontEndpointMap = new HashMap<String, List<String>>();
        ontEndpointMap.put("http://www.natural-europe.eu/ontology#", neEndpoints);
        ontEndpointMap.put("http://natural-europe.tuc.gr/sw/col/", colEndpoints);
        ontEndpointMap.put("http://purl.uniprot.org/core", uniprotEndpoints);
        ontEndpointMap.put("http://dbpedia.org/ontology/", dbpediaEndpoints);
        ontEndpointMap.put("http://www.geonames.org/ontology", geonamesEndpoints);

        // load the file location of the endpoint descriptions
        InputStream is = getClass().getClassLoader().getResourceAsStream("application.properties");
        if (is != null) {
            Properties p = new Properties();
            try {
                p.load(is);
                endpointDescLocation = p.getProperty("ENDPOINT_DESC_LOCATION");
            } catch (IOException ex) {
                log.warn("Configuration file failed to be loaded.", ex);
            }
        }

        // Natural Europe Ontology: http://natural-europe.tuc.gr/ne-ontology-v01.owl
        // Geonames Ontology: http://www.geonames.org/ontology/ontology_v3.1.rdf
        // Uniprot Ontology: ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/rdf/core.owl
    }

}
