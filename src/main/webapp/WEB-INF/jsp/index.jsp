<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>The SPARQL-RW Framework</title>
        <link rel="stylesheet" media="all" href="resources/js/codemirror/lib/codemirror.css"/>
        <link rel="stylesheet" media="all" href="resources/js/codemirror/addon/fold/foldgutter.css"/>
        <link rel="stylesheet" media="all" href="resources/js/codemirror/addon/hint/show-hint.css"/>
        <link rel="stylesheet" media="all" href="resources/js/bootstrap/bootstrap.min.css"/>
        <link rel="stylesheet" media="all" href="resources/js/bootstrap/bootstrap-theme.min.css"/>
        <link rel="stylesheet" media="all" href="resources/css/basestyle.css"/>
        <link rel="icon" type="image/png" href="resources/img/favicon.png">
        <script src="resources/js/codemirror/lib/codemirror.js"></script>
        <script src="resources/js/codemirror/mode/sparql/sparql.js"></script>
        <script src="resources/js/codemirror/addon/fold/foldcode.js"></script>
        <script src="resources/js/codemirror/addon/fold/foldgutter.js"></script>
        <script src="resources/js/codemirror/addon/fold/brace-fold.js"></script>
        <script src="resources/js/codemirror/addon/fold/xml-fold.js"></script>
        <script src="resources/js/codemirror/addon/fold/markdown-fold.js"></script>
        <script src="resources/js/codemirror/addon/fold/comment-fold.js"></script>
        <script src="resources/js/codemirror/addon/fold/indent-fold.js"></script>
        <script src="resources/js/codemirror/addon/edit/closebrackets.js"></script>
        <script src="resources/js/codemirror/addon/edit/matchbrackets.js"></script>
        <script src="resources/js/codemirror/addon/edit/trailingspace.js"></script>
        <script src="resources/js/codemirror/addon/selection/active-line.js"></script>
        <script src="resources/js/codemirror/addon/hint/show-hint.js"></script>
        <script src="resources/js/codemirror/addon/hint/anyword-hint.js"></script>
        <script src="resources/js/jquery/jquery-1.11.0.min.js"></script>
        <script src="resources/js/jquery/dynatable/jquery.dynatable.js"></script>
        <script src="resources/js/bootstrap/bootstrap.min.js"></script>
        <script src="resources/js/bootbox/bootbox.min.js"></script>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a href="index.show" class="navbar-brand">The SPARQL-RW Framework</a>
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-main">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="index.show">Demo Application</a></li>
                        <li><a href="about.show">About</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="page-header header-style" id="banner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h2>The SPARQL-RW Framework</h2>
                        <p class="lead">Case Study: Integrating the resources of Natural Europe with DBpedia.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="loadingDialog" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 id="loadingDialogMsg"></h3>
                        </div>
                        <div class="modal-footer">
                            <div class="progress progress-striped active">
                                <div class="progress-bar" style="width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="workspace" class="clearfix">
                <ul id="steps" class="nav nav-tabs">
                    <li class="active"><a href="#step1" data-toggle="tab">Step 1: Enter a Query</a></li>
                    <li><a href="#step2" data-toggle="tab">Step 2: Execute the Reformulated Query</a></li>
                    <li><a href="#step3" data-toggle="tab">Step 3: Inspect the Results</a></li>
                </ul>
                <div class="tab-content">
                    <div id="step1" class="tab-pane fade active in">
                        <p style="text-align:justify;">In this step you may express a SPARQL query in terms of our central schema or select a predefined one from the respective list. The input query can be reformulated by pressing the button on the bottom right corner of the form. Note that our data integration scenario involves the 6 Natural History Museum nodes of the <a href="http://www.natural-europe.eu/" target="_blank">Natural Europe project</a> and <a href="http://dbpedia.org/About" target="_blank">DBpedia</a>.</p>
                        <div class="row">
                            <div class="col-lg-12">
                                <form class="well form-horizontal">
                                    <fieldset>
                                        <legend>Select a predefined query or enter your own:</legend>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <select id="selectedQuery" class="form-control">
                                                    <option value="">Select a predefined query ...</option>
                                                    <option value="defaultQuery1">Find media objects of Natural Europe specimens whose species has been characterized as "Near Threatened".</option>
                                                    <option value="defaultQuery2">Find photos of near threatened species</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <p style="box-shadow: 0 0 1px black;"><textarea id="inputQuery"></textarea></p>
                                            </div>
                                        </div>
                                        <div class="form-group pull-right" style="margin-bottom: 0px;">
                                            <div class="col-lg-12">
                                                <button id="rewriteQuery" data-loading-text="Rewriting..." class="btn btn-primary">Rewrite the Query</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="step2" class="tab-pane fade">
                        <p style="text-align:justify;">In this step you may inspect the reformulated query, produced by the SPARQL-RW framework, before being executed. The resulted query has been generated based on a set of predefined mappings between our central schema and the schemas of the integrated data sources. Note that query execution depends totally on <a href="https://github.com/anapsid/anapsid" target="_blank">ANAPSID</a> and on a Java wrapper that we have implemented on top of it.</p>
                        <div class="row">
                            <div class="col-lg-12">
                                <form class="well form-horizontal">
                                    <fieldset>
                                        <legend>The reformulated query expressed in terms of the integrated data sources:</legend>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <p style="box-shadow: 0 0 1px black;"><textarea id="outputQuery"></textarea></p>
                                            </div>
                                        </div>
                                        <div class="form-group pull-right" style="margin-bottom: 0px;">
                                            <div class="col-lg-12">
                                                <button id="executeQuery" class="btn btn-primary">Execute the Query</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="step3" class="tab-pane fade">
                        <p style="padding-bottom: 0px; text-align:justify;">In this step you may inspect the results returned after executing the reformulated federated SPARQL query over the integrated data sources, namely the 6 <a href="http://www.natural-europe.eu/" target="_blank">Natural Europe</a> repositories and <a href="http://dbpedia.org/About" target="_blank">DBpedia</a>. You may search for specific values in the result set using the search box which is above the top right corner of the table, or you may sort the results by clicking on the table headers.</p>
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="dynatableHeaders" class="table-responsive">
                                </div>
                            </div>
                            <div class="row">
                                <div id="logos" class="col-lg-12">
                                    <a href="http://www.mnhnc.ulisboa.pt/portal/page?_pageid=418,1&_dad=portal&_schema=PORTAL" target="_blank"><img src="resources/img/logos/mnhnl.gif" width="190"/></a>
                                    <a href="http://www.nhmc.uoc.gr/" target="_blank"><img src="resources/img/logos/nhmc.gif" width="60"/></a>
                                    <a href="http://www.nhmus.hu/en" target="_blank"><img src="resources/img/logos/hnhm.gif" width="60"/></a>
                                    <a href="http://an01118.hp.altmuehlnet.de/index.php/de/" target="_blank"><img src="resources/img/logos/jme.gif" width="90"/></a>
                                    <a href="http://www.arcticcentre.org/" target="_blank"><img src="resources/img/logos/ac.gif" width="60"/></a>
                                    <a href="http://www.loodusmuuseum.ee/en/" target="_blank"><img src="resources/img/logos/tnhm.gif" width="60"/></a>
                                    <a href="http://dbpedia.org/About" target="_blank"><img src="resources/img/logos/dbpedia.png" width="90"/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer-style" role="contentinfo">
                <div class="row">
                    <div class="col-lg-12">
                        <p>Made by <a href="http://www.music.tuc.gr/Person.show?ID=244" rel="nofollow">Konstantinos Makris</a>. Based on <a href="http://getbootstrap.com" rel="nofollow">Bootstrap</a>, <a href="https://www.dynatable.com/" rel="nofollow">Dynatable</a> and <a href="http://codemirror.net/" rel="nofollow">Codemirror</a>.</p>
                        <p>Laboratory of Distributed Multimedia Information Systems & Applications <a href="http://www.music.tuc.gr" rel="nofollow">TUC/MUSIC</a>.</p>
                        <p><a href="http://www.ece.tuc.gr/">School of Electronic & Computer Engineering.</a> <a href="http://www.tuc.gr" rel="nofollow">Technical University of Crete</a></p>
                    </div>
                </div>
            </footer>
        </div>
        <script>

            $(document).ready(function() {
                var inputQueryEditor = editor(document.getElementById("inputQuery"));
                var outputQueryEditor = editor(document.getElementById("outputQuery"));
                var defaultQuery1 = "PREFIX G: <http://www.music.tuc.gr/sparqlrw/ne/mediator#>\n" +
                        "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n\n" +
                        "SELECT ?speciesName ?mediaObject\n" +
                        "WHERE {\n" +
                        "  ?specimen rdf:type G:Specimen .\n" +
                        "  ?specimen G:mediaObject ?mediaObject .\n" +
                        "  ?specimen G:species ?species .\n" +
                        "  ?species G:speciesName ?speciesName .\n" +
                        "  ?species G:conservation \"NT\"@en .\n" +
                        "}";
                var defaultQuery2 = "PREFIX G: <http://www.music.tuc.gr/sparqlrw/ne/mediator#>\n" +
                        "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n\n" +
                        "SELECT ?specimen ?conservation\n" +
                        "WHERE {\n" +
                        "  ?specimen rdf:type G:Specimen .\n" +
                        "  ?specimen G:species ?species .\n" +
                        "  ?species G:conservation ?conservation .\n" +
                        "}";

                // handles the submission of the form in step 1
                $("#rewriteQuery").click(function() {
                    var btn = $(this);
                    var query = inputQueryEditor.getDoc().getValue();
                    var json = {"query": query};
                    btn.button("loading");
                    $.ajax({
                        type: "POST",
                        url: "rewriteQuery",
                        data: JSON.stringify(json),
                        contentType: "application/json; charset=UTF-8",
                        dataType: "json",
                        beforeSend: function() {
                            if (query === "") {
                                bootbox.alert("Please enter a query or select a predefined one.");
                                btn.button("reset");
                                return false;
                            }
                        },
                        success: function(result) {
                            if (result.query === null) {
                                bootbox.alert("The query failed to be reformulated. Please check the query syntax.");
                            } else {
                                var respContent = "";
                                respContent += result.query;
                                outputQueryEditor.getDoc().setValue(respContent);
                                $("#steps li:eq(1) a").tab("show");
                            }
                        },
                        error: function() {
                            bootbox.alert("The query failed to be reformulated. Please check the query syntax.");
                        },
                        complete: function() {
                            btn.button("reset");
                        }
                    });
                    return false;
                });

                // handles the submission of the form in step 2
                $("#executeQuery").click(function() {
                    var query = outputQueryEditor.getDoc().getValue();
                    var json = {"query": query};
                    $.ajax({
                        type: "POST",
                        url: "executeQuery",
                        data: JSON.stringify(json),
                        contentType: "application/json; charset=UTF-8",
                        dataType: "json",
                        beforeSend: function() {
                            if (query === "") {
                                bootbox.alert("There is no query to execute. Please return to first step.");
                                return false;
                            }
                            $("#loadingDialogMsg").html("Executing...");
                            $("#loadingDialog").modal("show");
                        },
                        success: function(result) {
                            var headers = "<table id='resultsTable' class='table table-bordered table-hover'><thead><tr>";

                            if (result.varList.length === 0) {
                                return;
                            }

                            for (var i = 0; i < result.varList.length; i++) {
                                headers += "<th>" + result.varList[i] + "</th>";
                            }

                            headers += "</tr></thead><tbody></tbody></table>";

                            if (result.id === null) {
                                setTimeout(function() {
                                    // hide modal after 1 second
                                    $("#loadingDialog").modal("hide");
                                }, 1000);

                                setTimeout(function() {
                                    // show failure message after 1 second
                                    bootbox.alert("The query failed to be executed.");
                                }, 1000);
                            } else {
                                $("#dynatableHeaders").html(headers);
                                fetchResults(result.id);
                            }
                        },
                        error: function() {
                            setTimeout(function() {
                                // hide modal after 1 second
                                $("#loadingDialog").modal("hide");
                            }, 1000);

                            setTimeout(function() {
                                // show failure message after 1 second
                                bootbox.alert("The query failed to be executed.");
                            }, 1000);
                        }
                    });
                    return false;
                });

                // handles the drop-down list of the form in step 1 and puts the
                // respective predefined query in the codemirror editor
                $("#selectedQuery").on("change", function() {
                    if ($(this).val() === "defaultQuery1") {
                        inputQueryEditor.getDoc().setValue(defaultQuery1);
                    } else if ($(this).val() === "defaultQuery2") {
                        inputQueryEditor.getDoc().setValue(defaultQuery2);
                    } else if ($(this).val() === "defaultQuery3") {
                        inputQueryEditor.getDoc().setValue(defaultQuery3);
                    } else {
                        inputQueryEditor.getDoc().setValue("");
                    }
                });

                // handles the tabs
                $("#steps a").click(function(e) {
                    e.preventDefault();
                    $(this).tab("show");
                });

                // handles the refreshing of the codemirror editor values in
                // case they are not already rendered
                $("a[data-toggle='tab']").on("shown.bs.tab", function() {
                    inputQueryEditor.refresh();
                    outputQueryEditor.refresh();
                });

                // fetches the results to be presented in the dynatable
                function fetchResults(id) {
                    // fetch the results using ajax
                    $.ajax({
                        url: "fetchResults/" + id,
                        beforeSend: function() {
                            $("#loadingDialogMsg").html("Fetching results...");
                        },
                        success: function(data) {
                            $("#resultsTable").dynatable({
                                features: {
                                    // leave the browser url unchanged
                                    pushState: false
                                },
                                dataset: {
                                    records: data
                                },
                                params: {
                                    records: "_root"
                                },
                                input: {
                                    paginationClass: "pagination pagination-sm"
                                },
                                writers: {
                                    _attributeWriter: function(record) {
                                        var value = record[this.id];
                                        if (isUrl(value)) {
                                            return "<a target=\"_blank\" href=\"" + value + "\">" + value + "</a>";
                                        } else {
                                            return value;
                                        }
                                    }
                                }
                            });
                        },
                        complete: function() {
                            $("#steps li:eq(2) a").tab("show");
                            $("#loadingDialog").modal("hide");
                        }
                    });
                }

                // checks if a given value is a url
                function isUrl(s) {
                    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
                    return regexp.test(s);
                }

                // creates a codemirror editor in the specified dom location
                function editor(id) {
                    return CodeMirror.fromTextArea(id, {
                        mode: "sparql",
                        // theme: "lesser-dark",
                        lineNumbers: true,
                        lineWrapping: true,
                        autoCloseBrackets: true,
                        matchBrackets: true,
                        showTrailingSpace: false,
                        styleActiveLine: true,
                        extraKeys: {"Ctrl-Q": function(cm) {
                                cm.foldCode(cm.getCursor());
                            }, "Ctrl-Space": function(cm) {
                                CodeMirror.showHint(cm, CodeMirror.hint.anyword);
                            }},
                        foldGutter: true,
                        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
                    });
                }
            });
        </script>
    </body>
</html>
