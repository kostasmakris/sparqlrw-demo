<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>The SPARQL-RW Framework</title>
        <link rel="stylesheet" media="all" href="resources/js/bootstrap/bootstrap.min.css"/>
        <link rel="stylesheet" media="all" href="resources/js/bootstrap/bootstrap-theme.min.css"/>
        <link rel="stylesheet" media="all" href="resources/css/basestyle.css"/>
        <link rel="icon" type="image/png" href="resources/img/favicon.png">
        <script src="resources/js/jquery/jquery-1.11.0.min.js"></script>
        <script src="resources/js/bootstrap/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a href="index.show" class="navbar-brand">The SPARQL-RW Framework</a>
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="navbar-collapse collapse" id="navbar-main">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="index.show">Demo Application</a></li>
                        <li class="active"><a href="about.show">About</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="page-header header-style" id="banner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <h2>The SPARQL-RW Framework</h2>
                        <p class="lead">Transparent query access over mapped RDF datasets in the Web of Linked Data.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">Key Features</div>
                    <div class="panel-body">
                        <div class="container">
                            <ul>
                                <li class="col-sm-6">DL-based Ontology Mapping Model.</li>
                                <li class="col-sm-6">Support for Flexible & Rich Mapping Types.</li>
                                <li class="col-sm-6">Mapping Inferencing & Inconsistency Identification.</li>
                                <li class="col-sm-6">XML based Mapping Language.</li>
                                <li class="col-sm-6">SPARQL Query ReWriting based on Mappings.</li>
                                <li class="col-sm-6">Inference Support over SPARQL ReWriting.</li>
                                <li class="col-sm-6">Semantics Preserving Query ReWriting.</li>
                                <li class="col-sm-6">Java System Prototype.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">Publications</div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li style="text-align:justify;">Makris K., Bikakis N., Gioldasis N., Christodoulakis S.: "<a href="http://www.music.tuc.gr/GetFile?FILE_TYPE=PUB.FILE&FILE_ID=398" target="_blank">SPARQL-RW: Transparent Query Access over Mapped RDF Data Sources</a>". 15th International Conference on Extending Database Technology <em>(EDBT '12)</em>, Berlin, Germany, March 2012.</li>
                            <li style="padding-top: 12px; text-align:justify;">Makris K., Gioldasis N., Bikakis N., Christodoulakis S.: "<a href="http://www.music.tuc.gr/GetFile?FILE_TYPE=PUB.FILE&FILE_ID=350" target="_blank">Ontology Mapping and SPARQL Rewriting for Querying Federated RDF Data Sources</a>". 9th International Conference on Ontologies, DataBases, and Applications of Semantics <em>(ODBASE'10)</em>, Crete, Greece, October 2010.</li>
                            <li style="padding-top: 12px; text-align:justify;">Makris K.: "<a href="http://www.music.tuc.gr/GetFile?FILE_TYPE=PUB.FILE&FILE_ID=349" target="_blank">SPARQL Rewriting for Query Mediation over Mapped Ontologies</a>". Diploma Thesis, School of Electronic and Computer Engineering, Technical University of Crete, Greece, July 2010.</li>
                            <li style="padding-top: 12px; text-align:justify;">Makris K., Bikakis N., Gioldasis N., Tsinaraki C., Christodoulakis S.: "<a href="http://www.music.tuc.gr/GetFile?FILE_TYPE=PUB.FILE&FILE_ID=342" target="_blank">Towards a Mediator based on OWL and SPARQL</a>". 2nd World Summit on the Knowledge Society <em>(WSKS '09)</em>, Crete, Greece, September 2009.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">People</div>
                    <div class="panel-body">
                        <a href="http://www.music.tuc.gr/Person.show?ID=244" target="_blank">Konstantinos Makris</a> &nbsp; &#8226; &nbsp;
                        <a href="http://www.music.tuc.gr/Person.show?ID=5" target="_blank">Nektarios Gioldasis</a> &nbsp; &#8226; &nbsp;
                        <a href="http://www.dblab.ntua.gr/~bikakis" target="_blank">Nikos Bikakis</a> &nbsp; &#8226; &nbsp;
                        <a href="http://www.music.tuc.gr/Faculty.show?ID=2" target="_blank">Prof. Stavros Christodoulakis</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">Acknowledgements</div>
                    <div class="panel-body">
                        This work has been carried out in the scope of the Natural Europe Project (Grant Agreement 250579) funded by EU ICT Policy Support Programme.
                    </div>
                </div>
            </div>
        </div>
    <footer class="footer-style" role="contentinfo">
        <div class="row">
            <div class="col-lg-12">
                <p>Made by <a href="http://www.music.tuc.gr/Person.show?ID=244" rel="nofollow">Konstantinos Makris</a>. Based on <a href="http://getbootstrap.com" rel="nofollow">Bootstrap</a>, <a href="https://www.dynatable.com/" rel="nofollow">Dynatable</a> and <a href="http://codemirror.net/" rel="nofollow">Codemirror</a>.</p>
                <p>Laboratory of Distributed Multimedia Information Systems & Applications <a href="http://www.music.tuc.gr" rel="nofollow">TUC/MUSIC</a>.</p>
                <p><a href="http://www.ece.tuc.gr/">School of Electronic & Computer Engineering.</a> <a href="http://www.tuc.gr" rel="nofollow">Technical University of Crete</a></p>
            </div>
        </div>
    </footer>
</body>
</html>
