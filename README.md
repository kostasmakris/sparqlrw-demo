# README #

This is a mediator system implementation offering transparent query access over federated RDF data sources. It serves as a demo application for the *SPARQL–RW Framework*. The system is able to answer SPARQL queries expressed in terms of a global ontology schema, using information from a set of integrated data sources. Functionality is exposed through a web user interface and server-side logic has been implemented using the Spring MVC Framework.

![mediator-architecture-generic.png](https://bitbucket.org/repo/pgEekq/images/3945298287-mediator-architecture-generic.png)

The system employs SPARQL–RW both for mapping management, as well as for the rewriting and decomposition of SPARQL queries submitted to the mediator. Query rewriting is based on a set of predefined GAV ontology mappings between the global (mediator) schema and the schemas of the integrated data sources, while query decomposition relies on the available data source endpoints. 

For query execution and results merging, the mediator employs ANAPSID. ANAPSID is an open source query engine for SPARQL endpoints, developed at Universidad Simón Bolívar. ANAPSID adapts query execution schedulers to data availability and run-time conditions. It provides physical SPARQL operators that detect when a source becomes blocked or data traffic is bursty, and opportunistically, the operators produce results as quickly as data arrives from the sources.

*More information about the SPARQL-RW Framework is available at: [http://dias.library.tuc.gr/view/17491](http://dias.library.tuc.gr/view/17491)*